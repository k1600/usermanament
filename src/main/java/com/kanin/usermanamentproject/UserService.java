/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kanin.usermanamentproject;

import java.util.ArrayList;

/**
 *
 * @author User
 */
public class UserService {
    private ArrayList<User> userList;
    private int lastId = 1;
    
    public UserService() {
        userList = new ArrayList<User>();
    }
    
    public User addUser(User newUser){
        newUser.setId(lastId++);
        userList.add(newUser);
        return newUser;
    }
    
    public User getUser(int index){
        return userList.get(index);
    }
    
    public ArrayList<User> getUsers(){
        return userList;
    }
    public int getSize(){
        return userList.size();
    }
    public void logUserList(){
        for(User u : userList){
            System.out.println(u);
        }
    }

    User updateUser(int index, User upDatedUser) {
        User user = userList.get(index);
        user.setLogin(upDatedUser.getLogin());
        user.setName(upDatedUser.getName());
        user.setPassword(upDatedUser.getPassword());
        user.setRole(upDatedUser.getRole());
        user.setGender(upDatedUser.getGender());
        return user;
    }

    User deletUser(int index) {
        return userList.remove(index);
    }
}