/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.kanin.usermanamentproject;

import java.io.Serializable;

/**
 *
 * @author Jessada
 */
public class User  implements Serializable{
    private int id;
    private String login;
    private String name;
    private String password;
    private char gender;
    private char role;
    public User(String login, String name, String password, char gender, char role) {
       this(-1,login,name,password,gender,role);
    }

    public User(int id, String login, String name, String password, char gender, char role) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.password = password;
        this.gender = gender;
        this.role = role;
    }

    User() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getGenderString() {
        if (gender == 'M') {
            return "Male";
        } else if (gender == 'F') {
            return "Female";
        }
        return "";
    }

    public String getRoleString() {
        if (role == 'A') {
            return "Admin";
        } else if (role == 'U') {
            return "User";
        }
        return "";
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public char getRole() {
        return role;
    }

    public void setRole(char role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", login=" + login + ", name=" + name + ", password=" + password + ", gender=" + gender + ", role=" + role + '}';
    }
    
    
    
    
    
    
    

}
