/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
import com.kanin.usermanamentproject.User;
import com.kanin.usermanamentproject.UserService;
import java.util.ArrayList;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author User
 */
public class UserServiceTest {
    UserService userSerVice;
    public UserServiceTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
        userSerVice = new UserService();
        User newAdin1 = new User("admin1", "administrator", "pass@1234", 'M', 'A');
        User newUser1 = new User("User 1", "User 1", "pass@1234", 'M', 'U');
        User newUser2 = new User("User 2", "User 2", "pass@1234", 'F', 'U');
        User newUser3 = new User("User 3", "User 3", "pass@1234", 'M', 'U');
        userSerVice.addUser(newAdin1);
        userSerVice.addUser(newUser1);
        userSerVice.addUser(newUser2);
        userSerVice.addUser(newUser3);
        
    }

    @AfterEach
    public void tearDown() {
    }

    /**
     * Test of addUser method, of class UserService.
     */
    @Test
    public void testAddUser() {
        System.out.println("addUser");
        User newUser = new User("admin", "administrator", "pass@1234", 'M', 'A');
        UserService instance = new UserService();
        User expResult = newUser;
        User result = instance.addUser(newUser);
        assertEquals(expResult, result);
        assertEquals(1, result.getId());
    }

    /**
     * Test of getUser method, of class UserService.
     */
    @Test
    public void testGetUser() {
        System.out.println("getUser");
        int index = 1;        
        String expResult = "User 1";
        User result = userSerVice.getUser(index);
        assertEquals(expResult, result.getLogin());
    }

    /**
     * Test of getUsers method, of class UserService.
     */
    @Test
    public void testGetUsers() {
        System.out.println("getUsers");
        ArrayList<User> userList = userSerVice.getUsers();
        int expResult = 4;
        assertEquals(expResult, userList.size());
    }

    /**
     * Test of getSize method, of class UserService.
     */
    @Test
    public void testGetSize() {
        System.out.println("getSize");
        int expResult = 4;
        int result = userSerVice.getSize();
        assertEquals(expResult, result);
    }

}
